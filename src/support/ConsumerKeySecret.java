package support;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.Charsets;

/**
 * Container for a pair of consumer key and secret. See
 * https://dev.twitter.com/docs/auth/application-only-auth
 */
public class ConsumerKeySecret {

    private final String key;
    private final String secret;

    public ConsumerKeySecret(String key, String secret) {
        this.key = key;
        this.secret = secret;
    }

    public String getKey() {
        return key;
    }

    public String getSecret() {
        return secret;
    }

    private String getBearerToken() {
        return key + ":" + secret;
    }

    /**
     * @return Base64-encoded bearer token that can should be included in the
     * Authorization header for application-only requests.
     */
    public String getBearerTokenBase64() {
        return Base64.encodeBase64String(getBearerToken().getBytes(Charsets.UTF_8));
    }
}
