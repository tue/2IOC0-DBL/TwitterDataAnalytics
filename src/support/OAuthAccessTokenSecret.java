package support;

/**
 * Contains an OAuth token and secret pair. These tokens can be retrieved from
 * dev.twitter.com (https://dev.twitter.com/docs/auth/tokens-devtwittercom) or
 * by querying the user for a specific PIN. See {@link mining.OAuthRequester}
 * for the latter method.
 *
 * @author Peter Wu
 */
public class OAuthAccessTokenSecret {

    private final String token;
    private final String secret;

    public OAuthAccessTokenSecret(String token, String secret) {
        assert token != null;
        assert secret != null;
        this.token = token;
        this.secret = secret;
    }

    public String getToken() {
        return token;
    }

    public String getSecret() {
        return secret;
    }
}
