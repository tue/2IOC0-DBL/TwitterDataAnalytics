package support;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

/**
 * Tracks classes by name and whether those classes are enabled or not.
 *
 * @author Peter Wu
 * @param <T> The type of classes that are to be tracked.
 */
public class ClassEnabledTracker<T> {

    private final Map<String, Class<? extends T>> allowedClasses;
    private final Map<String, Boolean> enabledClasses;

    public ClassEnabledTracker(Map<String, Class<? extends T>> allowedClasses) {
        this.allowedClasses = allowedClasses;
        this.enabledClasses = new TreeMap<>();
        for (String name : getNames()) {
            enabledClasses.put(name, false);
        }
    }

    public Set<String> getNames() {
        return allowedClasses.keySet();
    }

    public Set<String> getEnabled() {
        Set<String> enabled = new TreeSet<>();
        for (Map.Entry<String, Boolean> e : enabledClasses.entrySet()) {
            if (e.getValue()) {
                enabled.add(e.getKey());
            }
        }
        return enabled;
    }

    public Set<String> getDisabled() {
        Set<String> disabled = new TreeSet<>(getNames());
        disabled.removeAll(getEnabled());
        return disabled;
    }

    public String getNameByClass(Class<T> aClass) {
        for (Map.Entry<String, Class<? extends T>> e : allowedClasses.entrySet()) {
            if (e.getValue() == aClass) {
                return e.getKey();
            }
        }
        return null;
    }

    public Class<? extends T> getClassByName(String name) {
        return allowedClasses.get(name);
    }

    public boolean has(String name) {
        return allowedClasses.containsKey(name);
    }

    public void disable(String name) {
        enabledClasses.put(name, false);
    }

    public void enable(String name) {
        enabledClasses.put(name, true);
    }

    public void disableAll() {
        setEnabledAll(false);
    }

    public void enableAll() {
        setEnabledAll(true);
    }

    private void setEnabledAll(boolean enabled) {
        for (Map.Entry<String, Boolean> e : enabledClasses.entrySet()) {
            e.setValue(enabled);
        }
    }

    public void enableClasses(List<T> classes) {
        for (T t : classes) {
            String name = getNameByClass((Class<T>) t.getClass());
            if (name != null) {
                enabledClasses.put(name, true);
            }
        }
    }
}
