package mining;

import java.io.IOException;
import java.util.Set;

/**
 * Provides access to a stream. The implementor is supposed to provide means
 * that allows a client to be notified of new tweets.
 */
public interface Stream {

    /**
     * Add the keyword (or phrase) to the list of keywords to be watched in a
     * stream.
     *
     * @param keyword
     */
    public void watchKeyword(String keyword);

    /**
     * Removes the keyword (or phrase) from the list of keywords to watch for in
     * a stream.
     *
     * @param keyword
     */
    public void unwatchKeyword(String keyword);

    /**
     * Starts streaming tweets. If a connection is already open, then it may be
     * re-opened to use new keywords.
     *
     * @throws IOException on failure to open a streaming connection.
     */
    public void commit() throws IOException;

    /**
     * Stops the stream. After this method has been called, no more messages
     * will be processed.
     */
    public void close();

    /**
     * Test whether the stream is ready for streaming
     *
     * @return true if connection can be made, false otherwise.
     */
    public boolean isValid();

    /**
     * Determine keywords for the search query.
     *
     * @param active true to return keywords actually in use, false to return
     * keywords that are queued for the next iteration.
     * @return A list of keywords, possibly empty if there are none.
     */
    public Set<String> getKeywords(boolean active);
}
