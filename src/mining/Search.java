package mining;

import provider.ExceptionListener;
import provider.ResultListener;

/**
 * An interface that defines the searching for tweets of a given user.
 *
 * @author Maurice Laveaux
 */
public interface Search {
    
    /**
     * Search for all history (up to 3200) tweets for a specific user.
     * 
     * @param screenName The profile to search tweets from.
     * @param numberOfTweets The maximum amount of tweets to search.
     */
    public void search(String screenName, int numberOfTweets);
    
    /**
     * Closes the threads for searching and saves a tempfile.
     */
    public void close();
    
    /**
     * Save the current work queue to a specific file.
     * 
     * @param filename The filename to save to.
     */
    public void save(String filename);
    
    /**
     * Load the work queue of a specific file.
     * 
     * @param filename The filename to load from.
     */
    public void load(String filename);
    
    /**
     * @param listener The listener for resulting tweets.
     */
    public void setResultListener(ResultListener listener);    
    
    /**
     * @param listener The listener for exceptions.
     */
    public void setExceptionListener(ExceptionListener listener);
}
