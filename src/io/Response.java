package io;

import com.google.gson.JsonElement;

/**
 * A response object with the Jsonelement and rate limit values.
 *
 * @author Maurice Laveaux
 */
public class Response {

    private final JsonElement element;

    private final long rateLimitReset;
    
    private final long timeRemaining;

    private final int rateLimitRemaining;

    private final int rateLimit;

    public Response(JsonElement resp, int rateLimit, int rateLimitRemaining, long rateLimitReset, long timeRemaining) {
        this.element = resp;
        this.rateLimit = rateLimit;
        this.rateLimitRemaining = rateLimitRemaining;
        this.rateLimitReset = rateLimitReset;
        this.timeRemaining = timeRemaining;
    }

    public JsonElement getJson() {
        return element;
    }

    public int getRateLimit() {
        return this.rateLimit;
    }

    public int getRateLimitRemaining() {
        return this.rateLimitRemaining;
    }

    public long getRateLimitReset() {
        return this.rateLimitReset;
    }

    /**
     * @return Time in milliseconds that should be waited for before a new
     * similar request can be executed.
     */
    public long getRateLimitRemainingTime() {
        return this.timeRemaining;
    }
}
