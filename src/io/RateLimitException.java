package io;

/**
 * The exception that will be thrown when a ratelimit has been hit.
 *
 * @author Maurice Laveaux
 */
public class RateLimitException extends Exception {

    private final long rateLimitReset;

    public RateLimitException(long resetTime) {
        rateLimitReset = resetTime;
    }

    public long getResetTime() {
        return rateLimitReset;
    }

    /**
     * @return Time in milliseconds that should be waited for before a new
     * similar request can be executed.
     */
    public long getRateLimitRemainingTime() {
        return rateLimitReset * 1000 - System.currentTimeMillis();
    }
}
