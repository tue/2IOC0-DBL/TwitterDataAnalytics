package io;

import java.io.IOException;

/**
 * Performs an API request.
 *
 * @author Peter Wu
 */
public interface Requester {

    /**
     * Performs an API request for a resource, for example
     * "statuses/mentions_timeline" (note, no leading slash nor ".json" suffix).
     * Prefix the resource with "1/" when you really need to target the old 1.0
     * API.
     *
     * @param resource The REST resource.
     * @return A JSON object resulting from the request.
     * @throws java.io.IOException on error fetching the resource.
     * @throws io.RateLimitException on ratelimit errors.
     */
    public Response getJSON(String resource) throws IOException, RateLimitException;

    /**
     * Tests whether this instance can dispatch requests.
     *
     * @return true if the access tokens are valid, false otherwise.
     * @throws java.io.IOException if the status cannot reliably be determined
     * (e.g. network error, parsing error).
     */
    public boolean isValid() throws IOException;
}
