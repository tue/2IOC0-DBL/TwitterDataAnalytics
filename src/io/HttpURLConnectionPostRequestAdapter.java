package io;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import oauth.signpost.basic.HttpURLConnectionRequestAdapter;
import org.apache.commons.io.Charsets;

/**
 * Adapts a HttpUrlConnection instance with the POST method for use with OAuth
 * signing.
 *
 * @author Peter Wu
 */
public class HttpURLConnectionPostRequestAdapter extends
        HttpURLConnectionRequestAdapter {

    private final String postData;

    public HttpURLConnectionPostRequestAdapter(HttpURLConnection connection,
            String postData) {
        super(connection);
        this.postData = postData;
    }

    @Override
    public InputStream getMessagePayload() throws IOException {
        // the super function returned null because it was unable to get the
        // post data...
        byte[] buf = postData.getBytes(Charsets.UTF_8);
        return new ByteArrayInputStream(buf);
    }
}
