package utils;

import io.DataWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import main.TweetShell;

/**
 * Global configuration including user-specific settings.
 */
public class Configuration {

    // The constumer secret value obtained from twitter.com
    public static final String CONSUMER_SECRET = "0aQLFiAwAgPHOYt6tziWoDftayQR7n3cUzMbGlhiNqsNNylLu4";
    // The constumer public value obtained from twitter.com
    public static final String CONSUMER_KEY = "1Qc8Y5kmWIZy13m0hVxgGJOPt";

    public static final String REQUEST_TOKEN_URL = "https://twitter.com/oauth/request_token";
    public static final String AUTHORIZE_URL = "https://twitter.com/oauth/authorize";
    public static final String ACCESS_TOKEN_URL = "https://twitter.com/oauth/access_token";

    public static final String DEFAULT_TWEETS_FILENAME = "tweets.txt";

    private static Properties properties;
    private final File storeFile;
    
    private static Configuration config;

    private Configuration(File storeFile) {
        this.storeFile = storeFile;
        properties = new Properties(getDefaults());
        try (FileInputStream fis = new FileInputStream(storeFile)) {
            properties.load(fis);
        } catch (FileNotFoundException ex) {
            /* ignore non-existing configuration files, will be written later */
        } catch (IOException ex) {
            System.err.println("Could not load configuration: " + ex);
        }
    }
    
    public static Configuration getConfig() {
        if (config == null) {
            config = new Configuration(new File("config.txt"));
        }        
        return config;
    }

    private Properties getDefaults() {
        Properties defs = new Properties();
        // set default preferences as needed

        defs.setProperty(DataWriter.CFG_TWEETS_FILENAME, "tweets.txt");
        defs.setProperty(TweetShell.CFG_CONVERT_UNCOMPRESSED, "true");

        return defs;
    }

    /**
     * Writes the configuration to file.
     */
    public void save() {
        try (FileOutputStream fos = new FileOutputStream(storeFile)) {
            properties.store(fos, "");
        } catch (IOException ex) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE,
                    "Failed to save prefs", ex);
        }
    }

    /**
     * Changes a setting with the given key.
     *
     * @param key The key of the setting.
     * @param val The new value for the setting.
     */
    public void setProperty(String key, String val) {
        properties.setProperty(key, val);
    }

    /**
     * Gets a setting with the given key.
     *
     * @param key Name of the configuration setting.
     * @return The value for the key or null if there is none.
     */
    public String getProperty(String key) {
        return getProperty(key, null);
    }

    /**
     * Gets a setting with the given key.
     *
     * @param key Name of the configuration setting.
     * @param defaultVal The default value for the key.
     * @return The value for the key or null if there is none.
     */
    public String getProperty(String key, String defaultVal) {
        return properties.getProperty(key, defaultVal);
    }
}
