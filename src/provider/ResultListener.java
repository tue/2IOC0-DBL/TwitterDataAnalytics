package provider;

import com.google.gson.JsonObject;

/**
 * Callback when a new tweet or user is received.
 */
public interface ResultListener {

    /**
     * This method is called when a new tweet is provided.
     *
     * @param obj A single JSON object.
     */
    public void tweetGenerated(JsonObject obj);
}
