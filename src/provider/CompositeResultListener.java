package provider;

import com.google.gson.JsonObject;
import java.io.Closeable;
import java.io.Flushable;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 * ResultListener that can contain multiple registrations.
 *
 * @author Peter Wu
 */
public class CompositeResultListener implements ResultListener, Closeable,
        Flushable {

    private final List<ResultListener> listeners;

    public CompositeResultListener() {
        this.listeners = new ArrayList<>();
    }

    public void register(ResultListener rl) {
        listeners.add(rl);
    }

    public void unregister(ResultListener rl) {
        listeners.remove(rl);
    }

    public List<ResultListener> getRegistered() {
        return new ArrayList<>(listeners);
    }

    public ResultListener findListener(Class<? extends ResultListener> rlCls) {
        for (ResultListener rl : listeners) {
            if (rlCls == rl.getClass()) {
                return rl;
            }
        }
        return null;
    }

    @Override
    public void tweetGenerated(JsonObject obj) {
        for (ResultListener rl : listeners) {
            rl.tweetGenerated(obj);
        }
    }

    @Override
    public void close() {
        for (ResultListener rl : listeners) {
            if (rl instanceof Closeable) {
                IOUtils.closeQuietly((Closeable) rl);
            }
        }
    }

    @Override
    public void flush() {
        for (ResultListener rl : listeners) {
            if (rl instanceof Flushable) {
                try {
                    ((Flushable) rl).flush();
                } catch (IOException ex) {
                    Logger.getLogger(rl.getClass().getName())
                            .log(Level.SEVERE, "flush", ex);
                }
            }
        }
    }
}
