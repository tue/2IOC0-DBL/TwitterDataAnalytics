package provider;

/**
 * Notifies whenever an unexpected, fatal exception occurred.
 */
public interface ExceptionListener {

    /**
     * Callback for fatal errors.
     *
     * @param ex Exception that occurred.
     */
    public void exceptionGenerated(Exception ex);
}
