package main;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import provider.ResultListener;

/**
 * Calculates statistic about the received tweets.
 *
 * @author Peter Wu
 */
public class TweetCounter implements ResultListener {

    private final static Logger LOGGER
            = Logger.getLogger(TweetCounter.class.getName());

    private int tweetCount = 0;
    private final Set<String> users;
    private final Date start_date;

    public TweetCounter() {
        this.users = new HashSet<>();
        this.start_date = new Date();
    }

    @Override
    public void tweetGenerated(JsonObject obj) {
        try {
            JsonObject userObj = obj.getAsJsonObject("user");
            String screen_name = userObj.get("screen_name").getAsString();
            tweetCount++;
            users.add(screen_name); 
        } catch (JsonParseException ex) {
            LOGGER.log(Level.WARNING, "Profile is missing data", ex);            
        }
    }

    public int getTweetCount() {
        return tweetCount;
    }

    /**
     * @return The set of users who tweeted. Do not modify its contents!
     */
    public Set<String> getUsers() {
        return users;
    }

    public Date getStartDate() {
        return start_date;
    }

    public String getActiveTime() {
        Date now = new Date();
        long timediff = (now.getTime() - start_date.getTime()) / 1000;
        return String.format("%d hour(s), %d min(s), %d sec(s)",
                timediff / 3600,
                (timediff % 3600) / 60,
                timediff % 60);
    }
}
