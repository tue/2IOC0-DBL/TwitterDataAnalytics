package main;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.DataWriter;
import io.RateLimitException;
import io.Response;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import mining.TwitterApi;
import utils.Configuration;

/**
 * Class for manually testing the Twitter API.
 */
public class Main {

    /**
     * Command and parameters without options.
     */
    private Command command;
    private String[] params;
    /**
     * Whether to use the Bearer method or OAuth-signed requests.
     */
    private boolean useBearer = true;
    private boolean rawOutput = false;
    private TwitterApi api_cached;

    public Main(String[] args) throws IOException {
        // parse options and command and return the parameters.
        parseGlobalOptions(args);
    }

    private TwitterApi getApi() throws IOException {
        if (api_cached == null) {
            if (useBearer) {
                api_cached = TwitterApi.getAppOnly();
            } else {
                api_cached = TwitterApi.getOAuth(new ConsolePinSupplier());
            }
        }
        return api_cached;
    }

    private String getParam(int index, String name) {
        if (index >= params.length) {
            System.err.println("Missing parameter: " + name);
            System.exit(1);
        }
        return params[index];
    }

    private void parseGlobalOptions(String[] args) {
        int firstParam = -1;
        /* parse global options */
        for (int i = 0; i < args.length; i++) {
            if ("--oauth".equals(args[i])) {
                useBearer = false;
            } else if ("--raw".equals(args[i])) {
                rawOutput = true;
            } else if ("--output".equals(args[i])) {
                i++;
                Configuration.getConfig().setProperty(DataWriter.CFG_TWEETS_FILENAME,args[i]);
                Configuration.getConfig().save();
            } else if (args[i].startsWith("-")) {
                throw new IllegalArgumentException("Invalid option: " + args[i]);
            } else {
                /* not an option, must be a command */
                if (args[i].startsWith("?")) {
                    command = Command.help;
                } else {
                    command = Command.fromString(args[i]);
                }
                firstParam = i + 1;
                break;
            }
        }
        if (firstParam == -1) {
            throw new IllegalArgumentException("Missing command, use \"help\"");
        }
        params = Arrays.copyOfRange(args, firstParam, args.length);
    }

    public static void main(String[] args) throws IOException {
        try {
            Main main = new Main(args);
            main.execute();
        } catch (IllegalArgumentException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }
    }

    enum Command {
        user,
        tweet,
        searchtweets,
        shell,
        hack,
        help;

        public static Command fromString(String command) {
            for (Command cmd : values()) {
                if (cmd.name().equals(command)) {
                    return cmd;
                }
            }
            throw new IllegalArgumentException("Unrecognized command ");
        }
    };

    private final static String[] HELP = {
        "Global options:",
        "   --oauth    Use OAuth (PIN) instead of Bearer tokens",
        "   --raw      Do not beautify JSON output",
        "   --output   Change the output filename",
        "",
        "Available commands:"
    };

    private void searchTweets(String q) throws IOException {
        try {
            TwitterApi.Builder req = getApi().build("search/tweets");
            req.param("q", q);
            req.param("count", "100"); // max number of tweets, cannot be higher
            req.param("lang", "en");
            Response resp = req.request();
            JsonArray statuses = resp.getJson().getAsJsonObject().get("statuses").getAsJsonArray();
            for (int i = 0; i < statuses.size(); i++) {
                JsonObject tweet = statuses.get(i).getAsJsonObject();
                System.out.println(tweet);
            }
        } catch (RateLimitException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void execute() throws IOException {
        TwitterApi.Builder req = null;
        /* build a request for commands */
        switch (command) {
            case user:
                req = getApi().build("users/show");
                req.param("screen_name", getParam(0, "screen name"));
                break;
            case tweet:
                req = getApi().build("statuses/show");
                req.param("id", getParam(0, "numerical ID of tweet"));
                break;
            case searchtweets:
                searchTweets(getParam(0, "search query"));
                /* no req, will be handled by search */
                break;
            case hack:
                String name = getParam(0, "resource name");
                req = getApi().build(name);
                if (name.startsWith("-")) {
                    throw new IllegalArgumentException("Resource expected, got option");
                }
                for (int i = 1; i < params.length; i++) {
                    String keyval[] = params[i].split("=", 2);
                    if (keyval.length == 2) {
                        req.param(keyval[0], keyval[1]);
                    } else {
                        req.param(keyval[0], "");
                    }
                }
                break;
            case help:
                for (String line : HELP) {
                    System.out.println(line);
                }
                for (Command cmd : Command.values()) {
                    System.out.println("  " + cmd.name());
                }
                break;
            case shell:
                TweetShell shell = new TweetShell();
                // pass any remaining parameters to the shell
                if (params.length > 0) {
                    for (String cmd : params) {
                        shell.execute(cmd);
                    }
                }
                shell.process_forever();
                break;
            default:
                throw new AssertionError(command.name());
        }
        if (req != null) {
            try {
                System.err.println("Executing: " + req.toString());
                Response result = req.request();
                System.out.println(result.getJson().toString());
            } catch (RateLimitException ex) {
                Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    private static class ConsolePinSupplier implements TwitterApi.PinSupplier {

        private final Scanner scanner;

        public ConsolePinSupplier() {
            scanner = new Scanner(System.in);
        }

        @Override
        public String requestPin(String url) throws IOException {
            System.out.println(url);
            System.err.println("Please open the above URL and enter PIN:");
            return scanner.nextLine();
        }
    }
}
