#!/bin/sh
# You should build the jar file with `ant jar`, then run this script

if [ -z "$CA" ]; then
    CA=/tmp/cap/.keystore
fi

# Proxy parameters from
# http://docs.oracle.com/javase/7/docs/technotes/guides/security/jsse/JSSERefGuide.html#Customization
proxyport=8008
proxy_options() {
    # Do not add proxy options if there is no CA or no_proxy env is set
    if [ ! -s "$CA" ] || [ -n "$no_proxy" ]; then
        return
    fi

    # If proxy is not running, don't enable it
    netstat -tnl | grep -q :8008 || return

    echo -Dhttps.proxyHost=localhost
    echo -Dhttps.proxyPort=$proxyport
    echo -Djavax.net.ssl.trustStore=$CA
    # Disable hostname verification
    premain=tools/DisableSSLHostnameVerify.jar
    if [ -e "$premain" ]; then
        echo -javaagent:$premain
    fi
}

# Exit on errors
set -e

# Change dir to project
cd "$(dirname "$(readlink -f "$0")")"

jar=dist/TwitterDataAnalytics.jar
# Build jar if missing
[ -e "$jar" ] || ant jar

java $(proxy_options) -jar "$jar" "$@"
